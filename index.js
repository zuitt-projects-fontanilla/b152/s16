

/*function displayMsgToSelf(pastSelf){
	console.log("Hello Past Self!")
}

displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
displayMsgToSelf(pastSelf)
*/

function showIntensityAlert(windSpeed) {
	try {
		//attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));

		//error/err are commonly used variable names used by the developers for storing errors
	} catch (error) {
		//"typeof" operator is used to check the date type of a value/expression and returns a string value
		//of what the data type is
		console.log(typeof error);

		//catch errors within "try" statement
		//in this case the error is an unknown function "alert" which does not exist in Javascript
		//the 'alert' function is used similarly to a prompt to alert the user
		//'error.message' is used to acces the information to an error object
		console.log(error.message);

	} finally {
		//continue the exection of code regardless of success and failure of code execution in the try block to 
		//handle/resolve errors.
		alert('Intensity updates will show new alert.');

	}
}
showIntensityAlert(56);

/*const num = 100, x = 'a';

console.log(num);
try {
	console.log(num/x);
	console.log(a);
} catch (error) {
	console.log("An error caught.");
	console.log("Error message:"" + error);
}

*/

/*function gradeEvaluator (grade) {
	if (grade <= 70){
		console.log("F")
	} else if (grade <= 79 && grade >= 71){
		console.log("C")
	} else if (grade <= 89 && grade >= 80){
		console.log("B")
	} else if (grade <= 100 && grade >= 90){
		console.log("A")	
	} try {
		grade = string
	} catch(error) {
		alert (error)
	} finally {
		alert ("Finished")
	}
}

gradeEvaluator (54);
gradeEvaluator(76);
gradeEvaluator (84);
gradeEvaluator (99);
gradeEvaluator ("a");*/


/* answer

function gradeEvaluator(grade){
	try {
		f
	}
}
*/

function displayMsgToSelf(){
	console.log("Don't text him back.")
}

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();

let count = 10;

while(count !==0) {
	displayMsgToSelf();
	count--;
}


//while loop allows us to repeat an action as long as the condition is true

/*

1st loop - count 10
2nd loop - count 9
3rd loop - count 8
4th loop - count 7

while loop checked if count is still NOT equal to 0:
at this point before a possible 11th loop, count is decremented to 0
therefore, there was no 11th loop

if there is no decrementation, the condition is always true, thus an infinite loop.

Infinite loos will run your block forever until you stop it.

an infinite loop is a piece of code that keeps running because a termniation condition is never reached. 
Tghus may cause your app, browser or even PC to crash.

Sample:

 while(true){
	console.log("This will result an infinite loop.")

 }

	Always make sure that at the very least your loop condition will be terminated or wil be false at one point.

*/



//Do While loop is similar to While loop. Though Do While loop will run the code
//block at least once

//While loop, we first check the condition and then run the code block/statements


let doWhileCounter = 20

do {
	console.log(doWhileCounter)
	--doWhileCounter
} while (doWhileCounter > 0)


//for loop
// a for loop is more flexible than while and do while loops.
//it consists of 3 conditions:
// 1. "initialization" value that will track the progression of the loop
// 2. "expression/ condition" that will be 4valuated which will determine the loop
//will run one more time.
// 3. The "finalExpression" indicated how to advance the loop



/*

	Syntax:

	for(initialization, condition, finalExpression){
	
		//codeblock / statement
	}


*/

 // Creat a loop that will start from 1 and end at 209

 for(let x =1; x<=20; x++){
 	console.log(x)

 }


 //Can we use for loop in a string?

 let name = "Nikko Handsome";

 //Accessing elements of a string
 	//using index
 	//index starts at 0

 console.log(name[0])
 console.log(name[1])
 console.log(name[2])


//count of the string characters
	//using.length
	console.log(name.length)

//use for loop to display each chartacter of the string
for(let index = 0; index < name.length; index++){
	console.log(name[index])
}

let fruits = ["mango", "apple", "orange", "banana", "strawberry", "Kiwi"]
// elements - each value inside the array
console.log(fruits)
//total count of elements in an array
console.log(fruits.length)

//Access each element in an array
console.log(fruits[0])//mango
console.log(fruits[4])//strawberry
console.log(fruits[5])//kiwi


//determin the last element in array if we don't know the total no of elements
//in an array
console.log(fruits[fruits.length - 1])
//  console.log(fruits[6])

//if we want to assign new value to an element 
fruits[6] = "grapes"
console.log(fruits)


//using for loop in an array
for(let i = 0; i < fruits.length; i++){
	console.log(fruits[i])
}

let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "LUxury Sedan",
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}


];

console.log(cars.length)
console.log(cars[1])

//Use for loop to display each element in the cars
//array


let y=0;

for(y;y < cars.length; y++){
	console.log (cars [y])
}



let myName = "AdrIAn madArang"

for(let i=0; i < myName.length; i++){
	if(
	   myName[i].toLowerCase() == "a" ||
	   myName[i].toLowerCase() == "e" ||
	   myName[i].toLowerCase() == "i" ||
	   myName[i].toLowerCase() == "o" ||
	   myName[i].toLowerCase() == "u" 
	
	){
		console.log(3)
	} else {
		console.log(myName[i])
	}
	
}

//Continue and Breaks statement
 //The "Continue" statement allows the code to go to the next iteration
 //of the loop w/o finishing the execution of the4 following statements
 //in a code block

 //break

 for(let a = 20; a > 0; a--){

 	if(a % 2 === 0){
 		continue//continue means skip
 	}
 	
 	console.log(a)	

 	if (a < 10){
 		break
 	}
 }
let string = "alexandro"

for(let i = 0; i < name.length; i++){
	if(string[i].toLowerCase() === "a"){
		console.log("continue to the next iteration")
		continue;
	}

	if (string[i].toLowerCase() === "d"){
		break;
	}
	
}

//nested loops

	for(let x = 0; x<= 10; x++){
		console.log(x)

		for (let y = 0; y <= 10; y++){
			console.log(`${x} + ${y}) = ${x+y}`)
		}
	}






